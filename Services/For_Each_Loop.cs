﻿using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2.Services
{
    class For_Each_Loop
    {
        public static void Number()
        {
            int i;
            Console.Write("\n\n");
            Console.Write("Display All odd numbers between 1 and 10");
            Console.Write("-----------------------------------------");
            Console.Write("\n\n");

            Console.WriteLine("The odd numberes between 1 and 10 are: ");
            Console.ReadLine();

            int[] Oddnumbers = { 1, 3, 5, 7, 9 };
           
            foreach(int o in Oddnumbers)
            {
                if (o <= 9)
                {
                    Console.WriteLine("This an odd number");
                }
                else
                {
                    Console.WriteLine("Invalid");
                }
                    
            }
        }
    }
}
