﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2.Services
{
    class WhileLoop
    {
        public static void Number()
        {
            int i;
            Console.Write("\n\n");
            Console.Write("Display the first ten natural numbers: \n");
            Console.Write("-----------------------------------------");
            Console.Write("\n\n");

            Console.WriteLine("The first ten natural numberes are: ");
            Console.ReadLine();

            i = 1;
            while (i <= 10)
            {
                Console.Write("{0} ", i);
            }
            Console.Write("\n\n");
            Console.ReadLine();
            i++;
        }
    }
}
