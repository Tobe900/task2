﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2.Services
{
    class For_Loop
    {
        public static void Number()
        {
            int i;
            Console.Write("\n\n");
            Console.Write("Display the first ten natural numbers: \n");
            Console.Write("-----------------------------------------");
            Console.Write("\n\n");

            Console.WriteLine("The first ten natural numberes are: ");
            Console.ReadLine();

            for (i = 1; i <= 10; i++)
            {
                Console.Write("{0} ", i);
            }
            Console.Write("\n\n");
            Console.ReadLine();
        }
    }
}
