﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task2.Services;

namespace Task2
{
    class Program
    {
        static void Main(string[] args)
        {
            var for_loop = new For_Loop();
            var for_each = new For_Each_Loop();
            var while_loop = new WhileLoop();
            var do_while_loop = new DoWhileLoop();
            var switched = new Switch();
            Console.Write("Correct");
            Console.ReadLine();
        }
    }
}
